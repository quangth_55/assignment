//
//  NetworkManager.swift
//  iTunesSearch
//
//  Created by QuangTH on 5/20/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import UIKit
@objc protocol NetworkManagerDelegate {
    optional func didReceivedData(songs: [NSObject])
    
}
class NetworkManager: NSObject {
    
    var delegate: NetworkManagerDelegate?
    func getSongForString(searchString: String, media: String) {
        let escapedString = searchString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())
        let url = NSURL(string: "https://itunes.apple.com/search?term=\(escapedString!)&media=\(media)")
        _ = NSURLSession.sharedSession().dataTaskWithURL(url!, completionHandler: { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if error == nil {
                let itunesDict = (try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)) as! NSDictionary
                let resultsArray = itunesDict.objectForKey("results") as! [Dictionary<String,AnyObject>]
                var datas:[MediaObject] = []
                if resultsArray.count > 0 {
                    for obj in resultsArray {
                        let track:String?
                        if obj["trackName"] != nil {
                            track = obj["trackName"] as? String
                        } else {
                            track = "Unknown"
                        }
                        let artist:String?
                        if obj["artistName"] != nil {
                            artist = obj["artistName"] as? String
                        } else {
                            artist = "Unknown"
                        }
                        let type:String?
                        if obj["kind"] != nil {
                            type = obj["kind"] as? String
                        } else {
                            type = "Unknown"
                        }
                        let image:String?
                        if obj["artworkUrl100"] != nil {
                            image = obj["artworkUrl100"] as? String
                        } else {
                            image = "Unknown"
                        }
                        let data = MediaObject(track: track!, artist: artist!, type: type!, image: image!)
                        datas.append(data)
                    }
                    self.delegate?.didReceivedData!(datas)
                }
            }
        }).resume()
    }
}
