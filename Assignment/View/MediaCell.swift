//
//  SongCell.swift
//  iTunesSearch
//
//  Created by QuangTH on 5/19/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import UIKit

class MediaCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var type: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
