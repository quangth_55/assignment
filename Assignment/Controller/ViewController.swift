//
//  ViewController.swift
//  Assignment
//
//  Created by QuangTH on 5/24/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import UIKit
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, PageViewControllerDelegate {
    
    var _receiveIndex = 0
    var receiveIndex: Int {
        get {
            return _receiveIndex
        }
        set {
            _receiveIndex = newValue
            let indexPath = NSIndexPath(forRow: _receiveIndex, inSection: 0)
            collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
            collectionView.reloadData()
        }
    }
    var transition: EasyTransition?
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: ViewController's lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //collectionView.delegate = self
        //collectionView.dataSource = self
        self.view.backgroundColor = UIColor(netHex: 0xc2f0f0)
        collectionView.backgroundColor = UIColor(netHex: 0x98e6e6)
        (childViewControllers[0] as! PageViewController).pageDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: Action handle
    @IBAction func swipeGes(sender: AnyObject) {
        let menuVC = storyboard!.instantiateViewControllerWithIdentifier("menu") as! UINavigationController
        transition = EasyTransition(attachedViewController: menuVC)
        transition?.transitionDuration = 0.4
        transition?.direction = .Left
        transition?.margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        transition?.sizeMax = CGSize(width: 250, height: CGFloat.max)
        transition?.zTransitionSize = 60
        transition?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
        presentViewController(menuVC, animated: true, completion: nil)
    }
    
    @IBAction func menu(sender: AnyObject) {
        let menuVC = storyboard!.instantiateViewControllerWithIdentifier("menu") as! UINavigationController
        transition = EasyTransition(attachedViewController: menuVC)
        transition?.transitionDuration = 0.4
        transition?.direction = .Left
        transition?.margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        transition?.sizeMax = CGSize(width: 250, height: CGFloat.max)
        transition?.zTransitionSize = 60
        transition?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
        presentViewController(menuVC, animated: true, completion: nil)
    }

    //MARK: UICollectionViewDataSource
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: (UIScreen.mainScreen().bounds.size.width - 40) / 4, height: 30)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return MediaType.types.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("collectionCell", forIndexPath: indexPath) as! CollectionViewCell
        cell.mediaLabel.text = MediaType.types[indexPath.row]
        cell.mediaLabel.layer.cornerRadius = 10
        cell.mediaLabel.clipsToBounds = true
        if receiveIndex == indexPath.row {
            cell.backgroundColor = UIColor(netHex: 0xbb99ff).colorWithAlphaComponent(0.5)
        } else {
            cell.backgroundColor = .clearColor()
        }
        return cell
    }
    
    //MARK: UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let selectedCell = collectionView.cellForItemAtIndexPath(indexPath)
        selectedCell?.backgroundColor = .orangeColor()
        let vc =  childViewControllers[0] as! PageViewController
        let temp = vc.indexPage
        receiveIndex = indexPath.row
        vc.currentIndexPage = indexPath.row
        vc.indexPage = indexPath.row
        if indexPath.row < temp {
            vc.setVCBefore()
        } else if indexPath.row > temp {
            vc.setVCAfter()
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let selectedCell = collectionView.cellForItemAtIndexPath(indexPath)
        selectedCell?.backgroundColor = .clearColor()
    }
    
    //MARK: PageViewController delegate
    func passIndex(index: Int) {
        self.receiveIndex = index
    }
}


