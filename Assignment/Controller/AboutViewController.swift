//
//  AboutViewController.swift
//  Assignment
//
//  Created by QuangTH on 6/6/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    //MARK: ViewController's lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Action handle
    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

}
