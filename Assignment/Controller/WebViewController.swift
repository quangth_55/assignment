//
//  WebViewViewController.swift
//  Assignment
//
//  Created by QuangTH on 6/3/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIGestureRecognizerDelegate {

    var transition: EasyTransition?
    @IBOutlet weak var webView: UIWebView!
    
    //MARK: ViewController's lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // load from url
        let ges = UIScreenEdgePanGestureRecognizer(target: self, action: "swipeGes")
        ges.delegate = self
        view.addGestureRecognizer(ges)
        let url = NSURL(string: "https://www.apple.com/itunes/")
        let request = NSURLRequest(URL: url!)
        webView.loadRequest(request)
        // load from local html file
//        let htmlFile = NSBundle.mainBundle().pathForResource("temp", ofType: "html")
//        let htmlString = try? String(contentsOfFile: htmlFile!, encoding: NSUTF8StringEncoding)
//        webView.loadHTMLString(htmlString!, baseURL: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //MARK: Action Handle
    @IBAction func menu(sender: AnyObject) {
        let menuVC = storyboard!.instantiateViewControllerWithIdentifier("menu") as! UINavigationController
        transition = EasyTransition(attachedViewController: menuVC)
        transition?.transitionDuration = 0.4
        transition?.direction = .Left
        transition?.margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        transition?.sizeMax = CGSize(width: 250, height: CGFloat.max)
        transition?.zTransitionSize = 60
        transition?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
        presentViewController(menuVC, animated: true, completion: nil)
    }
    
    
    @IBAction func swipeGes(sender: AnyObject) {
        let menuVC = storyboard!.instantiateViewControllerWithIdentifier("menu") as! UINavigationController
        transition = EasyTransition(attachedViewController: menuVC)
        transition?.transitionDuration = 0.4
        transition?.direction = .Left
        transition?.margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        transition?.sizeMax = CGSize(width: 250, height: CGFloat.max)
        transition?.zTransitionSize = 60
        transition?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
        presentViewController(menuVC, animated: true, completion: nil)
    }

}
