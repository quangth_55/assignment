//
//  DataViewController.swift
//  Assignment
//
//  Created by QuangTH on 5/25/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import UIKit

class DataViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NetworkManagerDelegate {

    var mvData = [MediaObject]()
    var indexType = 0
    var type = ""
    var postShown = [Bool](count: 50, repeatedValue: false)
    @IBOutlet weak var dataTableView: UITableView!
    
    //MARK: ViewController's lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let networkManager = NetworkManager()
        networkManager.delegate = self
        dataTableView.backgroundColor = UIColor(netHex: 0xc2f0f0)
        networkManager.getSongForString("Adele", media: type)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("dataCell", forIndexPath: indexPath) as! MediaCell
        cell.trackName.text = "Track Name: " + mvData[indexPath.row].trackName
        cell.artistName.text = "Artist Name: " + mvData[indexPath.row].artistName
        cell.type.text = "Type: " + mvData[indexPath.row].type
        if mvData[indexPath.row].image != "Unknown" {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                let url = NSURL(string: self.mvData[indexPath.row].image)
                if let imgData = NSData(contentsOfURL: url!) {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        cell.img.image = UIImage(data: imgData)
                        })
                } else {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    cell.img.image = UIImage(named: "")
                    })
                }
            })
        }
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor(netHex: 0xc2f0f0)
        } else {
            cell.backgroundColor = UIColor(netHex: 0x84e1e1)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mvData.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        //enable effect for the first appear
        if postShown[indexPath.row] {
            return
        }
        postShown[indexPath.row] = true
        // add rotation effect
        let rotationAngleInRadians = 90.0 * CGFloat(M_PI/180.0)
        let rotationTransform = CATransform3DMakeRotation(rotationAngleInRadians, 0, 0, 1)
        cell.layer.transform = rotationTransform
        // Define the final state (After the animation)
        UIView.animateWithDuration(0.5, animations: { cell.layer.transform = CATransform3DIdentity })
    }
    
    //MARK: Override fuction
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "show" {
            let destinationController = segue.destinationViewController as! DetailViewController
            if let indexPath = dataTableView.indexPathForSelectedRow {
                destinationController.img = mvData[indexPath.row].image
                destinationController.track = mvData[indexPath.row].trackName
                destinationController.artist = mvData[indexPath.row].artistName
            }
        }
    }
    
    //MARK: function to receive data
    func didReceivedData(datas: [NSObject]) {
        let newSongs = datas as! [MediaObject]
        self.mvData = newSongs
        dataTableView.reloadData()
    }

}
