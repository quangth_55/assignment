//
//  DetailViewController.swift
//  Assignment
//
//  Created by QuangTH on 5/30/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import UIKit
import Social

class DetailViewController: UIViewController {

    //MARK: outlets
    @IBOutlet weak var artistName: UILabel!
    var artist = ""
    @IBOutlet weak var trackName: UILabel!
    var track = ""
    @IBOutlet weak var image: UIImageView!
    var img = ""
    
    //MARK: ViewController's lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(netHex: 0xc2f0f0)
        trackName.text = track
        artistName.text = artist
        if img != "Unknown" {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                if let url = NSURL(string: self.img) {
                    let data = NSData(contentsOfURL: url)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.image.image = UIImage(data: data!)
                    })
                } else {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.image.image = UIImage(named: "images-2.jpeg")
                    })
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Action handle
    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func shareButton(sender: AnyObject) {
        var sharingItems = [AnyObject]()
        sharingItems.append(track)
        sharingItems.append(artist)
        sharingItems.append(image.image!)
        let activities = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        activities.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList,UIActivityTypeCopyToPasteboard,UIActivityTypeSaveToCameraRoll,UIActivityTypePrint,UIActivityTypeAssignToContact,UIActivityTypeCopyToPasteboard, UIActivityTypeMail, UIActivityTypePostToTencentWeibo, UIActivityTypePostToWeibo, UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo, UIActivityTypePostToFlickr, UIActivityTypeOpenInIBooks, UIActivityTypeMessage]
        presentViewController(activities, animated: true, completion: nil)
    }
}
