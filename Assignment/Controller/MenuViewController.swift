//
//  MenuViewController.swift
//  Assignment
//
//  Created by QuangTH on 5/25/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import UIKit
import MessageUI

class MenuViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    let sectionTitles = ["View", "More"]
    let sectionContents = [["Term of use", "Back To Top"], ["E-mail", "About"]]
    
    //MARK: ViewController's lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(netHex: 0xff6666)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: UITableViewControllerDataSource
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCellWithIdentifier("menuCell", forIndexPath: indexPath)
        cell.textLabel?.text = sectionContents[indexPath.section][indexPath.row]
        cell.backgroundColor = UIColor(netHex: 0xff6666)
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let webVC = mainStoryboard.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
                appDelegate.window?.rootViewController = webVC
                appDelegate.window?.makeKeyAndVisible()
            } else {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let mainVC = mainStoryboard.instantiateViewControllerWithIdentifier("MainViewController") as! ViewController
                appDelegate.window?.rootViewController = mainVC
                appDelegate.window?.makeKeyAndVisible()
            }
        case 1:
            if indexPath.row == 0 {
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients(["quangth55@gmail.com"])
                    mail.setSubject("Test Mail")
                    mail.setMessageBody("Hello", isHTML: false)
                    
                    presentViewController(mail, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "Error", message: "Can not send E-mail", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            } else {
                let aboutVC = self.storyboard?.instantiateViewControllerWithIdentifier("AboutViewController")
                presentViewController(aboutVC!, animated: true, completion: nil)
            }
        default:
            break
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    //MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
