//
//  PageViewController.swift
//  Assignment
//
//  Created by QuangTH on 5/25/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import UIKit
@objc protocol PageViewControllerDelegate {
    optional func passIndex(index: Int)
}

class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    var pageDelegate: PageViewControllerDelegate?
    var currentIndexPage = -1
    var indexPage = 0

    //MARK: ViewController's lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        if let firstVC = viewControllerAtIndex(0){
            setViewControllers([firstVC], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: UIPageViewControllerDataSource
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if indexPage == 0 || indexPage == NSNotFound {
            return nil
        }
        
        return viewControllerAtIndex(indexPage - 1)

    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        //var tempIndex = (viewController as! DataViewController).indexType
        if indexPage == NSNotFound {
            return nil
        }
        if indexPage == MediaType.types.count - 1 {
            return nil
        }
        
        return viewControllerAtIndex(indexPage + 1)
    }

    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return MediaType.types.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return currentIndexPage
    }

    //MARK: UIPageViewControllerDelegate
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        indexPage = currentIndexPage
        pageDelegate?.passIndex!(currentIndexPage)
    }
    
    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController]) {
        currentIndexPage = (pendingViewControllers[0] as! DataViewController).indexType
    }
    
    //MARK: handle fuctions
    func viewControllerAtIndex(index: Int) -> UIViewController? {
        let tableVC = storyboard?.instantiateViewControllerWithIdentifier("DataViewController") as! DataViewController
        tableVC.indexType = index
        switch index {
        case 0:
            tableVC.type = ""
            break
        case 1:
            tableVC.type = "musicVideo"
            break
        case 2:
            tableVC.type = "movie"
            break
        case 3:
            tableVC.type = "ebook"
            break
        case 4:
            tableVC.type = "audiobook"
            break
        case 5:
            tableVC.type = "podcast"
            break
        default:
            break
        }
        return tableVC
    }
    
    func setVCBefore() {
        if let vc = viewControllerAtIndex(indexPage){
            setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.Reverse, animated: true, completion: nil)
        }
    }
    
    func setVCAfter () {
        if let vc = viewControllerAtIndex(indexPage){
            setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        }

    }
}
