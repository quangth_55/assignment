//
//  MediaObject.swift
//  Assignment
//
//  Created by QuangTH on 5/25/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import Foundation
class MediaObject: NSObject {
    var trackName: String
    var artistName: String
    var type: String
    var image: String
    
    init(track: String, artist: String, type: String, image: String) {
        self.trackName = track
        self.artistName = artist
        self.type = type
        self.image = image
    }
}