//
//  MediaType.swift
//  Assignment
//
//  Created by QuangTH on 6/2/16.
//  Copyright © 2016 QuangTH. All rights reserved.
//

import Foundation
import UIKit
class MediaType {
    static let types = ["All", "Music Video", "Movie", "Ebook", "Audio Book", "Podcast"]
}